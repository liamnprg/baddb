use postgres::{Connection,TlsMode};
use colored::*;
use std::io;

#[derive(Debug)]
struct Person {
    id: i32,
    name: String,
}

type Name = String;

#[derive(Debug)]
enum Choice {
    Add(Name),
    Del(Name),
    Quit,
    ListAll,
}
fn main() {
    let conn = Connection::connect("postgresql://postgres@localhost:5432/person",TlsMode::None).unwrap();

    /*
    conn.execute(
        "INSERT INTO person (name)
                  VALUES (?1)",
        &[&me.name as &ToSql],
    ).unwrap();

    let mut stmt = conn
        .prepare("SELECT id, name FROM person")
        .unwrap();
    let person_iter = stmt
        .query_map(NO_PARAMS, |row| Person {
            id: row.get(0),
            name: row.get(1),
        }).unwrap();

    for person in person_iter {
        println!("Found person {:?}", person.unwrap());
    }*/
    loop {
        let answer = decide();
        println!("You chose {:?}", answer);
        match answer {
            Choice::Add(name) => add(&conn,name),
            Choice::Del(name) => del(&conn,name),
            Choice::Quit => break,
            Choice::ListAll => list_all(&conn),
        };
        
    }
    println!("{}","BYE".white().bold());
}

//super unimportant code :)
fn decide() -> Choice {
    println!("{}","What action do you want to take: (a)dd a user, (d)elete one, (L)ist all users or (q)uit.".red());

    //2nd one for the newline
    let mut buffer = String::new();
    //we use a single pop here because of the newline at the end :)))
    io::stdin().read_line(&mut buffer).unwrap();
    buffer.pop();

    match buffer.pop().unwrap() {
        'a' => Choice::Add(get_add()),
        'd' => Choice::Del(get_del()),
        'q' => Choice::Quit,
        'L' => Choice::ListAll,
        _ => Choice::ListAll,

    }
}

fn get_add() -> String {
    println!("{}","Who do you want to add? ".green());
    
    let mut buffer = String::new();
    io::stdin().read_line(&mut buffer).unwrap();
    buffer.pop().unwrap();
    buffer
}

fn get_del() -> String {
    println!("{}","What user do you want to lift the bannhammer against? ".green());
    let mut buffer = String::new();
    io::stdin().read_line(&mut buffer).unwrap();
    buffer.pop().unwrap();
    buffer
}

//begin importatnws





fn add(conn: &Connection,name: String) -> () {
   let sql = format!("insert into person(name) values ('{}')" , name);
   println!("{}",sql);
   let err = conn.execute(&sql,&[]);
   println!("{:?}", err);

}
fn del(conn: &Connection,name: String) -> () {
   let err = conn.execute("delete from person where name = $1",&[&name]);
   println!("{:?}", err);
}
fn list_all(conn: &Connection) -> () {

    for row in &conn.query("SELECT id, name FROM person", &[]).unwrap() {
        let id: i32 = row.get(0);
        let name: String = row.get(1);
        println!("id: {}, name: {}", id, name);
    }

}
